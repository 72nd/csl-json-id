package main

import (
	"github.com/urfave/cli"
	"gitlab.com/72th/csl-json-id/pkg"
	"log"
	"os"
)

func main() {
	app := cli.NewApp()
	app.Name = "csl-id"
	app.Usage = "Searches csl json files term and returns ids of matches"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "input, i",
			Usage: "path of input file",
		},
		cli.StringFlag{
			Name:  "term, t",
			Usage: "Term to search for",
		},
	}
	app.Action = run
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func run(c *cli.Context) error {
	cslJson := pkg.NewCslJson(c.String("input"), c.String("term"))
	cslJson.PrintMatchedList()
	return nil
}
