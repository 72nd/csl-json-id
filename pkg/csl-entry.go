package pkg

import (
	"fmt"
	"github.com/lithammer/fuzzysearch/fuzzy"
	"strings"
)

type CslFile []CslEntry

type CslEntry struct {
	Id string `json:"id"`
	Title string `json:"title"`
	Author []CslAuthor `json:"author"`
}

type CslAuthor struct {
	Family string `json:"family"`
	Given string `json:"given"`
}

func (e CslEntry) TryMatch(term string) bool {
	normalizedTerm := strings.ToLower(term)
	return fuzzy.Match(normalizedTerm, strings.ToLower(e.Title)) || fuzzy.Match(normalizedTerm, strings.ToLower(e.AuthorAsString()))
}

func (e CslEntry) AuthorAsString() string {
	var result string
	for i, author := range e.Author {
		delimiter := ";"
		if i == 0 {
			delimiter = ""
		}
		result = fmt.Sprintf("%s%s %s, %s", result, delimiter, author.Family, author.Given)
	}
	return result
}