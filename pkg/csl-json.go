package pkg

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
)

type CslJson struct {
	Content CslFile
	SearchTerm string
}

func NewCslJson(filePath, term string) *CslJson {
	return &CslJson{
		Content:   readAndParseJson(filePath),
		SearchTerm: term,
	}
}

func readAndParseJson(path string) CslFile {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		log.Panic(err)
	}

	m := CslFile{}
	err = json.Unmarshal(data, &m)
	if err != nil {
		log.Panic(err)
	}
	return m
}

func (c *CslJson) PrintMatchedList() {
	for _, entry := range c.Content {
		if !entry.TryMatch(c.SearchTerm) {
			continue
		}
		fmt.Printf("%s|%s|%s\n", entry.Id, entry.AuthorAsString(), entry.Title)
	}
}
